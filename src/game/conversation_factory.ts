import { sample } from "lodash";
import {
  CLOSING_QUESTION,
  GREETINGS,
  HIGH_LIKELIHOOD_RESULTS,
  LOW_LIKELIHOOD_RESULTS,
  withFeelings,
  YES_NO_RESPONSES,
} from "./statements";
import { CHARACTERS, CHARACTER_SYSTEM, CHARACTER_XANDER } from "./characters";
import {
  Character,
  Choice,
  Conversation,
  Feelings,
  GameState,
  Statement,
} from "./types";

const getRandomChoice = (
  statements: Statement[],
  ...feelings: Feelings[]
): Choice => {
  const statement = sample(
    feelings.length ? statements.filter(withFeelings(...feelings)) : statements
  ) as Statement;

  return {
    text: statement.messages[0],
    feelings: statement.feelings,
  };
};

const createFinalResultsConversation = (): Conversation => {
  const isLikely = Math.random() > 0.5;
  const message = isLikely
    ? sample(HIGH_LIKELIHOOD_RESULTS)!
    : sample(LOW_LIKELIHOOD_RESULTS)!;

  return {
    character: CHARACTER_XANDER,
    messages: [...message.messages, ...sample(CLOSING_QUESTION)!.messages],
    choices: [
      {
        text: "Goodbye.",
        feelings: [],
      },
      { text: "Goodbye!", feelings: [] },
      { text: "Goodbye...", feelings: [] },
    ],
    respond() {
      return null;
    },
  };
};

export const createFinalConversation = (game: GameState): Conversation => {
  return {
    character: CHARACTER_SYSTEM,
    messages: [
      "That will be all! How was it?",
      "Who would you like to conduct your exit interview?",
    ],
    choices: [CHARACTER_XANDER, CHARACTER_XANDER].map((x) => ({
      text: x.name,
      feelings: [],
    })),
    respond(choice) {
      return {
        character: CHARACTER_XANDER,
        messages: [
          ...sample(GREETINGS)!.messages,
          sample([
            "Ready to hear your results?",
            "How was it? Just like real life, huh? Ready to hear your results?",
            "Well, you were an unusual test subject. Ready to hear your results?",
            "I'd say the test results are obvious, but let's review the official report... Are you ready?",
          ])!,
        ],
        choices: [
          getRandomChoice(YES_NO_RESPONSES, "confident"),
          getRandomChoice(YES_NO_RESPONSES, "nervous"),
        ],
        respond(choice: Choice) {
          const conversation = createFinalResultsConversation();

          if (choice.feelings.includes("nervous")) {
            conversation.messages = [
              "Don't be afraid of the truth. It's there whether you see it or not.",
            ].concat(conversation.messages);
          }

          return conversation;
        },
      };
    },
  };
};

export const createPickCharacterConversation = (
  state: GameState,
  messages?: string[]
): Conversation => {
  return {
    character: CHARACTER_SYSTEM,
    messages: messages || ["Who would you like to interact with next?"],
    choices: state.currentPair.map((x) => ({
      text: x.name,
      payload: x,
      feelings: [],
    })),
    respond(choice) {
      const character = choice.payload as Character;

      return character.generateConversation(state);
    },
  };
};

export const createIntroConversation = (state: GameState): Conversation => {
  return {
    character: CHARACTER_SYSTEM,
    messages: ["Hello! Ready for the Turinske Test?"],
    choices: [
      getRandomChoice(YES_NO_RESPONSES, "confident"),
      getRandomChoice(YES_NO_RESPONSES, "nervous"),
    ],
    respond(choice) {
      const mainQuestion =
        "Which simulation being would you like to speak with first?";
      const message = choice.feelings.includes("nervous")
        ? [
            "Anxiety is a rational response for anyone... human or machine...",
            mainQuestion,
          ]
        : [`That's the spirit! ${mainQuestion}`];
      return createPickCharacterConversation(state, message);
    },
  };
};
