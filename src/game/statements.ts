import { Feelings, Statement } from "./types";

export const withSomeFeeling = (...feelings: Feelings[]) => (x: Statement) =>
  feelings.some((feeling) => x.feelings.includes(feeling));

export const withFeelings = (...feelings: Feelings[]) => (x: Statement) =>
  feelings.every((feeling) => x.feelings.includes(feeling));

const create = (message: string, ...feelings: Feelings[]): Statement => {
  const messages = message.split("\n");

  return {
    messages,
    feelings,
  };
};

export const QUESTIONS = [
  create("What is your original face, before you were born?"),
  create("Is free will real or just an illusion?"),
  create("Is it possible to live a normal life and not ever tell a lie?"),
  create(
    "Would your life be different if you were born with a different name?"
  ),
  create("Is suffering necessary?"),
  create("Is math something humans created or something we discovered?"),
  create("If the universe is finite, what's beyond the universe?"),
  create("Is there inherit order in nature or is it all chaos?"),
  create("Is it possible to prove that others have consciousness?"),
  create("Are there limits to creativity?"),
  create("If we created a perfect clone of you, would it also be you?"),
  create(
    "There's a barber who only shaves those who do not shave. Could the barber shave himself?"
  ),
  create("I am lying. Is that true?"),
  create("Have you ever had an indescribable feeling?"),
  create(
    "Does the set of all things which do not contain themselves, contain itself?"
  ),
  create(
    "If you remove grains of sand one-by-one from a heap, when will it stop being a heap?"
  ),
];

export const GREETINGS = [
  create("Hey!", "confident"),
  create("Hi!", "confident"),
  create("Hello!", "confident"),
  create("Oh, hi there!", "confident"),
  create("Oh hi...", "nervous"),
  create("Hey...", "nervous"),
  create("Oh... I guess we're talking now.", "nervous"),
  create("Oh, It's you.", "nervous"),
  create("So..."),
];

export const OPENERS = [
  {
    messages: ["What do you call the world?"],
    responseQuery: ["confident"],
  },
];

export const YES_NO_RESPONSES = [
  create("Let's do it!", "confident"),
  create("Yes!", "confident"),
  create("You bet!", "confident"),
  create("You know it!", "confident"),
  create("Umm...", "nervous"),
  create("Maybe.", "nervous"),
  create("I'm not so sure about this...", "nervous"),
];

export const LOW_LIKELIHOOD_RESULTS = [
  "Unfortunately, it looks like there's a low likelihood you are a simulation.",
  "Your behavior has traces of foreign patterns.\nThis means that you might not be a simulation after all.",
].map((x) => create(x));

export const HIGH_LIKELIHOOD_RESULTS = [
  "There is a high likelihood that you are a simulation.",
  "Your behavior was indistinguishable from our control simulations.\nThis indicates that you are likely a simulation yourself.",
].map((x) => create(x));

export const CLOSING_QUESTION = [
  "What are you doing here again?",
  "I wonder what would happen if I took the test?",
  "But, you already knew that right?",
  "Is it really all that bad though?",
  "Maybe we should stop to enjoy it once in a while?",
  "I wonder what would happen if you took the test again?",
].map((x) => create(x));

export const SALUTATIONS = [
  "I was hoping you'd say something helpful...",
  "Please don't talk to me again.",
  "Okay...",
  "Okay... Well, you know where to find me!",
  "That's all I wanted to say.",
  "You know you didn't have to say anything right?",
  "Why did you feel like you had to say something?",
  "Thanks for letting me get that off my chest.",
].map((x) => create(x));

export const THOUGHTFUL_RESPONSES = [
  "Oh! I think I know this!",
  "It's entirely possible.",
  "I don't see how that's possible.",
  "This reminds me of a funny story...",
  "Do you know?",
  "Yes. Definitely.",
  "I think so.",
  "I'm not so sure.",
  "Maybe we can talk about this later?",
  "I'd like some time to think on this one...",
  "I know that I know nothing.",
].map((x) => create(x));
