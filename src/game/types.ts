export type Feelings = "nervous" | "confident";

export type Statement = {
  messages: string[];
  feelings: Feelings[];
};

export type Character = {
  readonly id: string;
  readonly image?: string;
  readonly name: string;
  generateConversation(state: GameState): Conversation;
};

export type Choice = {
  text: string;
  payload?: any;
  feelings: Feelings[];
};

export type Conversation = {
  character: Character;
  messages: string[];
  choices: Choice[];
  respond?: (choice: Choice) => Conversation | null;
};

export type GameState = {
  readonly round: number;
  readonly pairs: Character[][];
  readonly currentPair: Character[];
  readonly choiceHistory: Choice[];
};

export type Game = {
  reset: () => void;
  incrementRound: () => void;
  pushChoice: (choice: Choice) => void;
} & GameState;
