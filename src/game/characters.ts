import { uniqueid } from "../utils";
import {
  DANIELLE_PNG,
  FOX_PNG,
  MAYA_PNG,
  PARROT_PNG,
  TYLER_PNG,
  XANDER_PNG,
} from "../assets";
import { Character, Conversation, Choice, GameState, Statement } from "./types";
import { sample } from "lodash";
import {
  GREETINGS,
  SALUTATIONS,
  QUESTIONS,
  THOUGHTFUL_RESPONSES,
} from "./statements";

const getRandomChoices = (statements: Statement[], count: number) => {
  const choices = new Array(count).fill(1).reduce((acc: Choice[]) => {
    const possibleChoices = statements.filter(
      (x) => !acc.some(({ text }) => text === x.messages[0])
    );

    if (possibleChoices.length) {
      const choice = sample(possibleChoices)!;
      const text = choice.messages[0];
      acc.push({ text, feelings: [] });
    }

    return acc;
  }, [] as Choice[]);

  return choices;
};

const getChoicesFromQuestion = (question: Statement, count: number) => {
  return getRandomChoices(THOUGHTFUL_RESPONSES, count);
};

const createDefaultCharacterConversation = (
  character: Character,
  game: GameState
): Conversation => {
  const question = sample(QUESTIONS)!;

  return {
    character,
    messages: [...sample(GREETINGS)!.messages, ...question?.messages],
    choices: getChoicesFromQuestion(question, 3),
    respond() {
      return {
        character,
        messages: sample(SALUTATIONS)!.messages,
        choices: [],
      };
    },
  };
};

const createCharacter = (
  name: string,
  image: string = "",
  createConversation = createDefaultCharacterConversation
): Character => {
  return {
    id: uniqueid(),
    image,
    name,
    generateConversation(game: GameState) {
      return createConversation(this, game);
    },
  };
};

export const CHARACTER_SYSTEM = createCharacter("System");
export const CHARACTER_DANIELLE = createCharacter("Danielle", DANIELLE_PNG);

export const CHARACTER_FOX = createCharacter("Fox", FOX_PNG, (character) => ({
  character,
  messages: ["...", "..."],
  choices: [
    { text: "...", feelings: [] },
    { text: "...", feelings: [] },
    { text: "...", feelings: [] },
  ],
  respond() {
    return {
      character,
      messages: ["..."],
      choices: [],
    };
  },
}));

export const CHARACTER_PARROT = createCharacter(
  "Parrot",
  PARROT_PNG,
  (character, game) => ({
    character,
    // The last choice would be Parrot, so we have to look to the previous one
    messages: [game.choiceHistory[game.choiceHistory.length - 2].text || "..."],
    choices: getRandomChoices(GREETINGS, 3),
    respond(choice) {
      return {
        character,
        messages: [choice.text],
        choices: [],
      };
    },
  })
);

export const CHARACTER_MAYA = createCharacter("Maya", MAYA_PNG);
export const CHARACTER_XANDER = createCharacter("Xander Lea", XANDER_PNG);
export const CHARACTER_TYLER = createCharacter("Tyler", TYLER_PNG);

export const CHARACTERS = [
  CHARACTER_XANDER,
  CHARACTER_TYLER,
  CHARACTER_DANIELLE,
  CHARACTER_FOX,
  CHARACTER_MAYA,
  CHARACTER_PARROT,
];
