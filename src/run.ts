import * as PIXI from "pixi.js";
import { shuffle } from "lodash";
import {
  createScenario,
  createDialogTypeout,
  createChoicesContainer,
} from "./components";
import { GAME_WIDTH, AVATAR_BG_HEIGHT } from "./constants";
import { EVENT_COMPLETE, EVENT_SKIP } from "./events";
import { waitForEvent, waitForTime } from "./utils";
import {
  Game,
  Conversation,
  Choice,
  createIntroConversation,
  createPickCharacterConversation,
  createGameState,
  createFinalConversation,
} from "./game";

export const runGame = async (app: PIXI.Application, root: PIXI.Container) => {
  let dialog: PIXI.Container | null = null;
  const scenarioView = createScenario();
  const choices = createChoicesContainer();

  choices.container.y = AVATAR_BG_HEIGHT;
  root.addChild(scenarioView.container);
  root.addChild(choices.container);

  root.interactive = true;
  root.on("pointerdown", () => {
    dialog?.emit(EVENT_SKIP);
  });

  const showDialog = (msg: string) => {
    if (dialog) {
      dialog.destroy();
      dialog = null;
    }

    dialog = createDialogTypeout(app, msg);
    dialog.y = 50;
    dialog.x = (GAME_WIDTH - dialog.width) / 2;

    scenarioView.container.addChild(dialog);
  };

  const showDialogAndWait = async (msg: string) => {
    showDialog(msg);
    await waitForEvent(dialog!, EVENT_COMPLETE);
  };

  const waitForChoice = async (): Promise<Choice> => {
    const response = await waitForEvent(choices.container, EVENT_COMPLETE);
    choices.clear();

    return response as Choice;
  };

  const waitForPauseOrSkip = (ms: number) =>
    Promise.race([waitForTime(ms), waitForEvent(dialog!, EVENT_SKIP)]);

  const showAllMessages = async (messages: string[]): Promise<void> => {
    if (messages.length === 0) {
      return;
    }
    if (!messages[0]) {
      return showAllMessages(messages.slice(1));
    }

    const [msg, ...rest] = messages;

    await showDialogAndWait(msg);

    if (rest.length) {
      // Give user time to read
      await waitForPauseOrSkip(4000);
      await showAllMessages(rest);
    }
  };

  const runConversation = async (game: Game, conversation: Conversation) => {
    // Clean from last convo
    choices.clear();

    scenarioView.setAvatar(conversation.character.image);
    await showAllMessages(conversation.messages);

    if (conversation.choices.length) {
      choices.showChoices(shuffle(conversation.choices));
      const choice = await waitForChoice();
      game.pushChoice(choice);
      return conversation.respond?.(choice);
    } else {
      await waitForPauseOrSkip(4000);
      return null;
    }
  };

  const run = async () => {
    let game = createGameState();
    let conversation = createIntroConversation(game);

    while (conversation) {
      const nextConversation = await runConversation(game, conversation);

      if (nextConversation) {
        conversation = nextConversation;
        continue;
      }

      // We have reached the end
      if (!game.currentPair) {
        break;
      }

      game.incrementRound();

      if (game.currentPair) {
        conversation = createPickCharacterConversation(game);
      } else {
        conversation = createFinalConversation(game);
      }
    }

    window.location.reload();
  };

  await run();
};
