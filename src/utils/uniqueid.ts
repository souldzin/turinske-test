const createUniqueIdFactory = () => {
  let id = 0;

  return () => (id++).toString();
};

export const uniqueid = createUniqueIdFactory();
