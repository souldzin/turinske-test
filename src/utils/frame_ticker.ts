import { Subject } from "rxjs";
import { scan, map, takeWhile } from "rxjs/operators";

export const createFrameTicker = ({ speed = 1, count = 1, isLoop = false }) => {
  const subject = new Subject<number>();
  const frame$ = subject
    .pipe(
      scan((acc, x) => {
        const nextVal = acc + x * speed;

        return isLoop ? nextVal % count : nextVal;
      }, 0)
    )
    .pipe(map(Math.floor))
    .pipe(takeWhile(isLoop ? () => true : (x) => x < count));

  const tick = (delta: number) => subject.next(delta);

  return {
    tick,
    frame$,
  };
};
