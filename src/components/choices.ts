import * as PIXI from "pixi.js";
import {
  GAME_WIDTH,
  CHOICES_HEIGHT,
  CHOICE_HEIGHT,
  CHOICE_WIDTH,
  CHOICE_PADDING,
  CHOICE_FONT_SIZE,
  CHOICE_BG,
  TEXT_BOX_SHADOW_HEIGHT,
} from "../constants";
import { EVENT_COMPLETE } from "../events";
import { Choice } from "../game";
import { createTextBox } from "./text_box";

type ChoiceChild = {
  choice: Choice;
  container: PIXI.Container;
};

const createChoice = (choice: Choice): ChoiceChild => {
  const container = new PIXI.Container();

  const { container: textContainer } = createTextBox({
    bgColor: CHOICE_BG,
    fontColor: 0xffffff,
    fontSize: CHOICE_FONT_SIZE,
    paddingX: 10,
    paddingY: 10,
    width: CHOICE_WIDTH,
    text: choice.text,
    minHeight: CHOICE_HEIGHT,
  });

  container.interactive = true;
  container.buttonMode = true;

  container.addChild(textContainer);

  return {
    container,
    choice,
  };
};

const createHoverGraphic = () => {
  const graphic = new PIXI.Graphics();

  graphic.visible = false;
  graphic.lineStyle(2, 0x000000);
  graphic.beginFill(0xffffff);
  graphic.drawPolygon([
    new PIXI.Point(0, 0),
    new PIXI.Point(20, 15),
    new PIXI.Point(0, 30),
  ]);
  graphic.endFill();

  return graphic;
};

export const createChoicesContainer = () => {
  const container = new PIXI.Container();
  const choicesContainer = new PIXI.Container();
  const hoverGraphic = createHoverGraphic();

  let choices: ChoiceChild[] = [];

  const choose = (child: ChoiceChild) => {
    container.emit(EVENT_COMPLETE, child.choice);
  };

  const drawHover = (index: number) => {
    if (index < 0 || index >= choices.length) {
      hoverGraphic.visible = false;
      return;
    }

    const child = choicesContainer.getChildAt(index) as PIXI.Container;
    hoverGraphic.visible = true;
    hoverGraphic.y =
      choicesContainer.y +
      child.y -
      hoverGraphic.height / 2 +
      (child.height - TEXT_BOX_SHADOW_HEIGHT) / 2;
    hoverGraphic.x = choicesContainer.x - 30;
  };

  const clear = () => {
    choicesContainer.removeChildren();
    choices = [];
    drawHover(-1);
  };

  const showChoices = (newChoices: Choice[]) => {
    clear();

    choices = newChoices.map(createChoice);

    choices.forEach((choice, index) => {
      const { container: childContainer } = choice;

      childContainer.on("pointerover", () => drawHover(index));
      childContainer.on("pointerout", () => drawHover(-1));
      childContainer.on("pointerdown", (e: any) => {
        e.stopPropagation();
        choose(choice);
      });
      choicesContainer.addChild(childContainer);
      childContainer.y = (childContainer.height + CHOICE_PADDING) * index;
    });

    choicesContainer.y = (CHOICES_HEIGHT - choicesContainer.height) / 2;
    choicesContainer.x = (GAME_WIDTH - choicesContainer.width) / 2;
  };

  container.addChild(choicesContainer);
  container.addChild(hoverGraphic);

  return {
    container,
    showChoices,
    clear,
  };
};
