import * as PIXI from "pixi.js";
import { Subscription } from "rxjs";
import { createDialog } from "./dialog";
import { createFrameTicker } from "../utils";
import { EVENT_COMPLETE, EVENT_SKIP } from "../events";

const PUNCTUATION = [".", "?", "!", ":"];
const PUNCTUATION_SET = new Set(PUNCTUATION);

export const createDialogTypeout = (app: PIXI.Application, msg: string) => {
  const ticker = app.ticker;
  const { container, setText: setDialogText } = createDialog();
  const { tick: frameTick, frame$ } = createFrameTicker({
    speed: 0.25,
    count: msg.length + 1,
    isLoop: false,
  });
  let subscription: Subscription | null = null;
  let onPunctuation = false;

  const updateText = (idx: number) => {
    onPunctuation =
      idx < msg.length &&
      PUNCTUATION_SET.has(msg[idx - 1]) &&
      !PUNCTUATION_SET.has(msg[idx]);

    const message = msg.slice(0, idx);
    setDialogText(message);
  };

  const onTick = (delta: number) => {
    frameTick(onPunctuation ? delta * 0.1 : delta);
  };

  const complete = () => {
    stopTicking();
    container.emit(EVENT_COMPLETE);
  };

  const skip = () => {
    onPunctuation = false;
    setDialogText(msg);
    complete();
  };

  const startTicking = () => {
    ticker.add(onTick);
    subscription?.unsubscribe();
    subscription = frame$.subscribe({
      next: updateText,
      complete,
    });
  };

  const stopTicking = () => {
    ticker.remove(onTick);
    subscription?.unsubscribe();
  };

  container.on("added", startTicking);
  container.on("removed", stopTicking);
  container.on(EVENT_SKIP, skip);

  return container;
};
