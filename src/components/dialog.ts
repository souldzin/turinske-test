import {
  DIALOG_WIDTH,
  DIALOG_BG,
  DIALOG_PADDING_X,
  DIALOG_PADDING_Y,
  DIALOG_FONT_SIZE,
  DIALOG_MIN_SIZE,
} from "../constants";
import { createTextBox } from "./text_box";

export const createDialog = (text = "") => {
  return createTextBox({
    bgColor: DIALOG_BG,
    fontColor: 0xffffff,
    width: DIALOG_WIDTH,
    fontSize: DIALOG_FONT_SIZE,
    minHeight: DIALOG_MIN_SIZE,
    paddingX: DIALOG_PADDING_X,
    paddingY: DIALOG_PADDING_Y,
    text,
  });
};
