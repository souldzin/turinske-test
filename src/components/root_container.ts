import * as PIXI from "pixi.js";
import {
  GAME_WIDTH,
  GAME_HEIGHT,
  GAME_RATIO,
  GAME_PADDING,
} from "../constants";
import { ViewSizeObservable } from "../utils";

export const createRootContainer = (size$: ViewSizeObservable) => {
  const rootContainer = new PIXI.Container();

  // Add empty sprite with width/height so that rootContainer
  // has width/height and will scale naturally.
  const bg = new PIXI.Sprite();
  bg.width = GAME_WIDTH;
  bg.height = GAME_HEIGHT;
  rootContainer.addChild(bg);

  size$.subscribe(({ width: widthArg, height: heightArg }) => {
    const width = widthArg - GAME_PADDING * 2;
    const height = heightArg - GAME_PADDING * 2;
    const newRatio = width / height;

    // new size is too wide
    if (newRatio > GAME_RATIO) {
      rootContainer.height = height;
      rootContainer.y = GAME_PADDING;
      rootContainer.width = height * GAME_RATIO;
      rootContainer.x = (width - rootContainer.width) / 2;
    } else if (newRatio < GAME_RATIO) {
      rootContainer.height = width * (1 / GAME_RATIO);
      rootContainer.y = GAME_PADDING;
      rootContainer.width = width;
      rootContainer.x = GAME_PADDING;
    } else {
      rootContainer.height = height;
      rootContainer.y = GAME_PADDING;
      rootContainer.width = width;
      rootContainer.x = GAME_PADDING;
    }
  });

  return rootContainer;
};
