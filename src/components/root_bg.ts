import * as PIXI from "pixi.js";
import { TILE_PNG } from "../assets";
import {
  GAME_WIDTH,
  GAME_HEIGHT,
  BG_TILE_SPEED,
  BG_TILE_SIZE,
} from "../constants";

export const createRootBackground = (ticker: PIXI.Ticker) => {
  const bg = PIXI.TilingSprite.from(TILE_PNG, {}) as PIXI.TilingSprite;
  bg.width = GAME_WIDTH;
  bg.height = GAME_HEIGHT;

  const onTick = (delta: any) => {
    const { x, y } = bg.tilePosition;
    const newX = (x - delta * BG_TILE_SPEED) % BG_TILE_SIZE;
    const newY = (y + delta * BG_TILE_SPEED) % BG_TILE_SIZE;

    bg.tilePosition.set(newX, newY);
  };

  bg.on("added", () => {
    ticker.add(onTick);
  });

  bg.on("removed", () => {
    ticker.remove(onTick);
  });

  return bg;
};
